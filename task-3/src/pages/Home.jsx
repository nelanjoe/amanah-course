import AddCatalogForm from "../components/AddCatalogForm";
import CatalogProducts from "../components/CatalogProducts";
import Navbar from "../components/Navbar";
import { getData } from "../utils/data";
import { useState, useEffect } from "react";

const Home = () => {
  const [products, setProducts] = useState([]);

  const fetchProducts = async () => {
    const allProducts = await getData(
      `https://api-project.amandemy.co.id/api/products`
    );

    setProducts(allProducts);
  };

  useEffect(() => {
    fetchProducts();
  }, []);
  return (
    <>
      <Navbar />
      <main className="max-w-[900px] m-auto my-10">
        <AddCatalogForm fetchProducts={fetchProducts} />
        <CatalogProducts products={products} />
      </main>
    </>
  );
};

export default Home;
