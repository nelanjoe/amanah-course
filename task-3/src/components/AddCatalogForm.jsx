import axios from "axios";
import { useState } from "react";

const AddCatalogForm = ({ fetchProducts }) => {
  const [formFields, setFormFields] = useState({
    name: "",
    stock: 0,
    harga: 0,
    is_diskon: false,
    harga_diskon: 0,
    category: "",
    image_url: "",
    description: "",
  });

  const resetFormFields = () => {
    setFormFields({
      name: "",
      stock: 0,
      harga: 0,
      is_diskon: false,
      harga_diskon: 0,
      category: "",
      image_url: "",
      description: "",
    });
  };

  const handleChange = (event) => {
    if (event.target.name === "name") {
      setFormFields({ ...formFields, name: event.target.value });
    } else if (event.target.name === "stock") {
      setFormFields({ ...formFields, stock: event.target.value });
    } else if (event.target.name === "harga") {
      setFormFields({ ...formFields, harga: event.target.value });
    } else if (event.target.name === "harga_diskon") {
      setFormFields({ ...formFields, harga_diskon: event.target.value });
    } else if (event.target.name === "is_diskon") {
      setFormFields({ ...formFields, is_diskon: event.target.chacked });
    } else if (event.target.name === "category") {
      setFormFields({ ...formFields, category: event.target.value });
    } else if (event.target.name === "image_url") {
      setFormFields({ ...formFields, image_url: event.target.value });
    } else if (event.target.name === "description") {
      setFormFields({ ...formFields, description: event.target.value });
    }

    console.log({ ...formFields });
  };

  const handleSubmit = async () => {
    try {
      const { data } = await axios.post(
        `http://api-project.amandemy.co.id/api/products`,
        {
          name: formFields.name,
          harga: formFields.harga,
          stock: formFields.stock,
          description: formFields.description,
          image_url: formFields.image_url,
          category: formFields.category,
          is_diskon: formFields.is_diskon,
          harga_diskon: formFields.harga_diskon,
        },
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      );

      console.log(data.info);

      fetchProducts();

      resetFormFields();

      alert("Berhasil Membuat Product");
    } catch (err) {
      alert(data.info);
      console.error(err);
    }
  };

  return (
    <div className="bg-white shadow-md p-8 rounded-md">
      <h2 className="text-2xl font-semibold text-blue-500 mb-4">
        Create Products
      </h2>
      <form className="space-y-5" method="POST" onSubmit={handleSubmit}>
        <div className="flex justify-between gap-7">
          <div className="flex flex-col w-full">
            <label className="mb-2">Nama Barang</label>
            <input
              type="text"
              name="name"
              value={formFields.name}
              onChange={handleChange}
              className="w-[100%] rounded-sm border border-black py-1 px-2"
              placeholder="Masukkan nama barang"
            />
          </div>
          <div className="flex flex-col w-full">
            <label className="mb-2">Stock Barang</label>
            <input
              type="number"
              name="stock"
              value={formFields.stock}
              onChange={handleChange}
              className="w-[100%] rounded-sm border border-black py-1 px-2"
              placeholder="0"
            />
          </div>
        </div>
        <div className="flex justify-between gap-7 items-center">
          <div className="flex flex-col w-full">
            <label className="mb-2">Harga Barang</label>
            <input
              type="number"
              name="harga"
              value={formFields.harga}
              onChange={handleChange}
              className="w-[100%] rounded-sm border border-black py-1 px-2"
              placeholder="0"
            />
          </div>
          <div className="w-full flex gap-2 items-center -mb-7">
            <input
              type="checkbox"
              name="is_diskon"
              chacked={formFields.is_diskon}
              onChange={handleChange}
            />
            <label>Status Diskon</label>
          </div>
        </div>
        <div className="flex justify-between gap-7 items-center">
          <div className="flex flex-col w-full">
            <label className="mb-2">Harga Diskon</label>
            <input
              type="number"
              name="harga_diskon"
              value={formFields.harga_diskon}
              onChange={handleChange}
              className="w-[100%] rounded-sm border border-black py-1 px-2"
              placeholder="0"
            />
          </div>
        </div>
        <div className="flex justify-between gap-7 flex-col">
          <div className="flex flex-col w-full">
            <label className="mb-2">Kategori Barang</label>
            <select
              name="category"
              id="kategori-barang"
              className="border border-black py-[5px] px-2"
              value={formFields.category}
              onChange={handleChange}
            >
              <option value="" disabled>
                --Pilih Kategori--
              </option>
              <option value="teknologi">Teknologi</option>
              <option value="makanan">Mananan</option>
              <option value="minuman">Minuman</option>
              <option value="hiburan">Hiburan</option>
              <option value="kendaraan">Kendaraan</option>
            </select>
          </div>
          <div className="flex flex-col w-full">
            <label className="mb-2">Gambar Barang</label>
            <input
              type="text"
              name="image_url"
              className="w-[100%] rounded-sm border border-black py-1 px-2"
              placeholder="Masukan image url"
              value={formFields.image_url}
              onChange={handleChange}
            />
          </div>
          <div className="flex flex-col w-full">
            <label className="mb-2">Description</label>
            <textarea
              name="description"
              className="border border-black"
              value={formFields.description}
              onChange={handleChange}
            />
          </div>
        </div>
        <div className="flex justify-end gap-2">
          <button className="px-4 py-2 bg-blue-500 text-white font-semibold rounded-sm mt-4">
            Add Product
          </button>
          <button className="px-4 py-2 bg-white text-blue-500 border border-blue-500 font-semibold rounded-sm  mt-4">
            Cancel
          </button>
        </div>
      </form>
    </div>
  );
};

export default AddCatalogForm;
