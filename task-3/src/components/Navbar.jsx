const Navbar = () => {
  return (
    <div className="flex justify-between items-center bg-white shadow-md px-6 py-4 sticky">
      <nav>
        <h2 className="font-semibold text-2xl text-blue-500">
          Catalog Products
        </h2>
      </nav>
      <div>
        <ul className="flex space-x-10">
          <li>
            <a href="" className="text-blue-500 font-semibold">
              Home
            </a>
          </li>
          <li>
            <a href="" className="text-blue-500 font-semibold">
              Forms
            </a>
          </li>
          <li>
            <a href="" className="text-blue-500 font-semibold">
              Products
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default Navbar;
