const Product = ({ name, harga_display, image_url, stock, is_diskon }) => {
  return (
    <div className="bg-white shadow-md overflow-hidden rounded-md">
      <img
        src={image_url}
        className="object-cover w-[200px] h-[180px]"
        alt={name}
      />
      <div className="p-2 space-y-1">
        <h1
          className={`text-xl capitalize ${
            is_diskon === true ? "font-semibold" : "font-normal"
          }`}
        >
          {name}
        </h1>
        <p>
          <span className="text-red-400 font-medium">{harga_display}</span>
        </p>
        <p className="text-blue-500 font-medium">Stock {stock}</p>
      </div>
    </div>
  );
};

export default Product;
