import Products from "./Products";

const CatalogProducts = ({ products }) => {
  return (
    <div className="mt-10">
      <h1 className="font-semibold text-2xl text-blue-500 mb-5">
        Catalog Products
      </h1>
      <Products products={products} />
    </div>
  );
};

export default CatalogProducts;
