import Product from "./Product";

const Products = ({ products }) => {
  return (
    <div className="flex flex-wrap justify-between gap-5 w-full">
      {products.map((product) => (
        <Product key={product.id} {...product} />
      ))}
    </div>
  );
};

export default Products;
