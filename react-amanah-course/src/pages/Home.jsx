import { useContext, useEffect } from "react";
import { NavLink } from "react-router-dom";
import { ArticleContext } from "../context/ArticleContext";
import { ProductContext } from "../context/ProductContext";

const Home = () => {
  const {
    fetchArticles,
    articles,
    loading: loadingArticle,
  } = useContext(ArticleContext);

  const {
    getAllProducts,
    products,
    loading: loadingProduct,
  } = useContext(ProductContext);

  useEffect(() => {
    fetchArticles();
    getAllProducts();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (loadingArticle || loadingProduct) {
    <div>
      <h1>Loading Fetch Data...</h1>
    </div>;
  }

  return (
    <section className="mt-8">
      <section>
        <h1 className="my-4">List Articles</h1>
        <div className="flex flex-row flex-wrap justify-center gap-4">
          {articles?.map((article) => (
            <div
              key={article?.id}
              className="shadow-md rounded-md px-4 py-2 w-full md:w-[300px] h-full space-y-2"
            >
              <div>
                <img
                  src={article?.image_url}
                  alt={article?.name}
                  className="max-w-full rounded-md"
                />
              </div>
              <h3 className="font-semibold text-xl truncate capitalize">
                {article?.name}
              </h3>
              <p className="truncate">{article?.content}</p>
              <div>
                <NavLink to={`/article/${article?.id}`}>See Detail</NavLink>
              </div>
            </div>
          ))}
        </div>
      </section>
      <section>
        <h1 className="my-4">List Products</h1>
        <div className="flex flex-row flex-wrap justify-center gap-4">
          {products?.map((product) => (
            <div
              key={product?.id}
              className="shadow-md rounded-md px-4 py-2 w-full md:w-[300px] h-full space-y-2"
            >
              <div>
                <img
                  src={product?.image_url}
                  alt={product?.name}
                  className="max-w-full rounded-md"
                />
              </div>
              <h3 className="font-semibold text-xl truncate capitalize">
                {product?.name}
              </h3>
              <p className="truncate">{product?.content}</p>
              <div>
                <NavLink to={`/product/${product?.id}`}>See Detail</NavLink>
              </div>
            </div>
          ))}
        </div>
      </section>
    </section>
  );
};

export default Home;
