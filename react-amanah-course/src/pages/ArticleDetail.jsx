import { useContext } from "react";
import { useParams } from "react-router-dom";
import { ArticleContext } from "../context/ArticleContext";
import { useEffect } from "react";
import { NavLink } from "react-router-dom";

const ArticleDetail = () => {
  const { id } = useParams();

  const { article, getArticle } = useContext(ArticleContext);

  useEffect(() => {
    getArticle(id);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <section className="mt-8">
      <div className="shadow-md rounded-md px-4 py-2 w-full md:w-fit mx-auto space-y-2">
        <img
          src={article?.image_url}
          alt={article?.name}
          className="w-full md:max-w-md rounded-md"
        />
        <h2 className="font-semibold text-2xl capitalize">{article?.name}</h2>
        <p>{article?.content}</p>
        <div>
          <NavLink to="/">Back to Home</NavLink>
        </div>
      </div>
    </section>
  );
};

export default ArticleDetail;
