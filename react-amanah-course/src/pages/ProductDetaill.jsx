import { useContext } from "react";
import { useParams } from "react-router-dom";
import { useEffect } from "react";
import { NavLink } from "react-router-dom";
import { ProductContext } from "../context/ProductContext";

const ProductDetail = () => {
  const { id } = useParams();

  const { product, getProductById } = useContext(ProductContext);

  useEffect(() => {
    getProductById(id);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <section className="mt-8">
      <div className="shadow-md rounded-md px-4 py-2 w-full md:w-fit mx-auto space-y-2">
        <img
          src={product?.image_url}
          alt={product?.name}
          className="w-full md:max-w-md rounded-md"
        />
        <h2 className="font-semibold text-2xl capitalize">{product?.name}</h2>
        <p className={product?.is_diskon && "line-through text-red-500"}>
          {product?.harga_display}
        </p>
        <p className="font-semibold text-lg">{product?.harga_diskon_display}</p>
        <p>Stock: {product?.stock}</p>
        <div>
          <NavLink to="/">Back to Home</NavLink>
        </div>
      </div>
    </section>
  );
};

export default ProductDetail;
