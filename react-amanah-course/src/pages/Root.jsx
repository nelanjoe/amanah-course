import { Outlet } from "react-router-dom";
import Navbar from "../components/Navbar";

const Root = () => {
  return (
    <>
      <Navbar />
      <main className="max-w-7xl mx-4 md:mx-auto">
        <Outlet />
      </main>
    </>
  );
};

export default Root;
