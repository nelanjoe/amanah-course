import ReactDOM from "react-dom/client";
import App from "./App.jsx";
import "./index.css";
import ArticleProvider from "./context/ArticleContext.jsx";
import ProductProvider from "./context/ProductContext.jsx";

ReactDOM.createRoot(document.getElementById("root")).render(
  <ArticleProvider>
    <ProductProvider>
      <App />
    </ProductProvider>
  </ArticleProvider>
);
