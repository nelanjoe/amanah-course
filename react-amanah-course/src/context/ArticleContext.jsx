import axios from "axios";
import PropTypes from "prop-types";
import { useState } from "react";
import { createContext } from "react";

export const ArticleContext = createContext();

const ArticleProvider = ({ children }) => {
  const [articles, setArticles] = useState([]);
  const [article, setArticle] = useState([]);
  const [loading, setLoading] = useState(false);

  const fetchArticles = async () => {
    setLoading(true);

    try {
      const token = localStorage.getItem("token");
      if (!token) return;

      const { data } = await axios.get(
        "https://api-project.amandemy.co.id/api/articles",
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );

      setArticles(data?.data);
      setLoading(false);
    } catch (err) {
      console.error(err);
      throw new Error(err);
    }
  };

  const getArticle = async (articleId) => {
    setLoading(true);
    try {
      const { data } = await axios.get(
        `https://api-project.amandemy.co.id/api/articles/${articleId}`
      );

      setArticle(data?.data);
      setLoading(false);
    } catch (err) {
      console.error(err);
      throw new Error(err);
    }
  };

  return (
    <ArticleContext.Provider
      value={{
        articles,
        setArticles,
        loading,
        setLoading,
        fetchArticles,
        article,
        getArticle,
      }}
    >
      {children}
    </ArticleContext.Provider>
  );
};

ArticleProvider.propTypes = {
  children: PropTypes.node,
};

export default ArticleProvider;
