import PropTypes from "prop-types";
import { createContext, useState } from "react";
import axios from "axios";

export const ProductContext = createContext();

const ProductProvider = ({ children }) => {
  const [products, setProducts] = useState([]);
  const [product, setProduct] = useState([]);
  const [loading, setLoading] = useState(false);

  const getAllProducts = async () => {
    setLoading(true);
    try {
      const token = localStorage.getItem("token");
      if (!token) return;

      const { data } = await axios.get(
        `https://api-project.amandemy.co.id/api/products`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );

      setProducts(data?.data);
      setLoading(false);
    } catch (err) {
      console.error(err);
      throw new Error(err);
    }
  };

  const getProductById = async (productId) => {
    setLoading(true);
    try {
      const token = localStorage.getItem("token");
      if (!token) return;

      const { data } = await axios.get(
        `https://api-project.amandemy.co.id/api/products/${productId}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );

      setProduct(data?.data);
      setLoading(false);
    } catch (err) {
      console.error(err);
      throw new Error(err);
    }
  };

  return (
    <ProductContext.Provider
      value={{
        products,
        product,
        getAllProducts,
        getProductById,
        loading,
      }}
    >
      {children}
    </ProductContext.Provider>
  );
};

ProductProvider.propTypes = {
  children: PropTypes.node,
};

export default ProductProvider;
