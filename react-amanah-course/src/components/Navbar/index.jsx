import axios from "axios";
import { useState } from "react";
import { NavLink, useNavigate } from "react-router-dom";

const Navbar = () => {
  const [isOpen, setIsOpen] = useState(false);
  const handleToggle = () => setIsOpen(!isOpen);

  const navigate = useNavigate();

  const token = localStorage.getItem("token");
  const user = JSON.parse(localStorage.getItem("user"));

  const handleLogout = async () => {
    try {
      axios.post(
        `https://api-project.amandemy.co.id/api/logout`,
        {},
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
    } catch (err) {
      console.error(err);
      throw new Error(err);
    } finally {
      localStorage.removeItem("token");
      localStorage.removeItem("user");
      navigate("/login");
    }
  };

  return (
    <header className="sticky z-30 top-0 bg-white bg-opacity-20 backdrop-filter backdrop-blur-lg border-b border-b-gray-200">
      <nav className="max-w-7xl mx-auto flex flex-row justify-between items-center h-16 px-4 xl:px-0">
        {user !== null ? (
          <h1>Welcome {user?.name}</h1>
        ) : (
          <NavLink className="font-semibold text-md" to="/">
            Home
          </NavLink>
        )}
        <div className="hidden sm:hidden md:flex md:flex-row md:space-x-4">
          {token !== null ? (
            <button className="font-semibold text-md" onClick={handleLogout}>
              Logout
            </button>
          ) : (
            <>
              <NavLink className="font-semibold text-md" to="/login">
                Login
              </NavLink>
              <NavLink className="font-semibold text-md" to="/register">
                Register
              </NavLink>
            </>
          )}
        </div>

        {/* Humberger Button */}
        <button className="md:hidden" onClick={handleToggle}>
          {!isOpen ? (
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth={1.5}
              stroke="currentColor"
              className="w-6 h-6"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5"
              />
            </svg>
          ) : (
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth={1.5}
              stroke="currentColor"
              className="w-6 h-6"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M6 18L18 6M6 6l12 12"
              />
            </svg>
          )}
        </button>
      </nav>
      {/* Mobile Nav */}
      {isOpen && (
        <div className="flex flex-col mx-4 mb-4 space-y-3 md:hidden">
          {token !== null ? (
            <button className="font-semibold text-md">Logout</button>
          ) : (
            <>
              <NavLink className="font-semibold text-md" to="/login">
                Login
              </NavLink>
              <NavLink className="font-semibold text-md" to="/register">
                Register
              </NavLink>
            </>
          )}
        </div>
      )}
    </header>
  );
};

export default Navbar;
