const sayHello = ["Hallo", "nama", "saya", "Nelan"];

/**
 * * Looping With Array Method ForEach
 * */
sayHello.forEach((hello) => {
  console.log(hello);
});

/**
 * * Looping With for of
 * */
for (const hello of sayHello) {
  console.log(hello);
}
