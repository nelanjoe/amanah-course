const hitungLuasPersegiPanjang = (alas, tinggi) => {
  return alas * tinggi;
};

const luas1 = hitungLuasPersegiPanjang(10, 5);
const luas2 = hitungLuasPersegiPanjang(2, 4);

console.log(luas1);
console.log(luas2);
