const person = {
  name: "Nelan",
  age: 22,
  address: "Bogor, Jawa Barat Indonesia",
};

/**
 * * Destructuring Object
 */
const { name, age, address } = person;

console.log(
  `Hallo nams saya ${name}, saya berumur ${age} tahun dan saya tinggal di ${address}`
);
