let numbers = [10, 14, 15, 2, 5];

/**
 * * Menghapus 1 index dibagian belakang
 * */
numbers.pop();

/**
 * * Menambahkan 2 index baru
 * */
numbers.push(8);
numbers.push(0);

/**
 * * Menghapus 2 index di urutan awal
 * */
numbers.shift();
numbers.shift();

/**
 * * Menambahkan 3, 7, 1 ke paling awal
 * */
numbers.splice(0, 0, 3, 7, 1);

console.log(numbers);
