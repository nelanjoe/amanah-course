const bodyMassIndex = (berat, tinggi) => {
  return berat / (tinggi * tinggi);
};

const result = bodyMassIndex(80, 1.8);

console.log(result);
