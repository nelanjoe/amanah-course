import { useSelector } from "react-redux";
import { Navigate, Outlet } from "react-router-dom";

import { selectedUserInfo } from "../../store/auth/auth.selector";

const PrivateRoute = () => {
  const userInfo = useSelector(selectedUserInfo);

  return userInfo ? <Outlet /> : <Navigate to="/login" replace={true} />;
};

export default PrivateRoute;
