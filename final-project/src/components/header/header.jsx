import { useDispatch, useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";

import { selectedUserInfo } from "../../store/auth/auth.selector";
import { logout } from "../../store/auth/auth.slice";

const Header = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const userInfo = useSelector(selectedUserInfo);

  const handleLogout = () => {
    dispatch(logout());
    navigate("/login", { replace: true });
  };

  return (
    <header className="sticky top-0 z-30 bg-white/20 backdrop-filter backdrop-blur-lg border-b border-b-gray-200">
      <nav className="max-w-5xl mx-auto">
        <div className="flex items-center justify-between h-16 px-4 md:px-0">
          <Link to="/" className="text-gray-900 font-semibold text-xl">
            Home
          </Link>
          <div className="flex flex-row items-center gap-x-4">
            {userInfo ? (
              <>
                <Link
                  to="/profile"
                  className="text-gray-900 font-semibold text-lg"
                >
                  Profile
                </Link>
                <button
                  onClick={handleLogout}
                  className="px-4 py-2 rounded-md text-white text-md font-medium bg-purple-600 hover:bg-purple-700"
                >
                  Logout
                </button>
              </>
            ) : (
              <>
                <Link
                  to="/register"
                  className="text-gray-900 font-semibold text-lg"
                >
                  Register
                </Link>
                <Link
                  to="/login"
                  className="text-gray-900 font-semibold text-lg"
                >
                  Login
                </Link>
              </>
            )}
          </div>
        </div>
      </nav>
    </header>
  );
};

export default Header;
