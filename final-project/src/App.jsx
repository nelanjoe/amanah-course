import {
  RouterProvider,
  createBrowserRouter,
  createRoutesFromElements,
  Route,
} from "react-router-dom";

import PrivateRoute from "./components/private-route/private-route";

import Root from "./pages/Root/root";
import Home from "./pages/home/home";
import ArticleDetail from "./pages/article-detail/article-detail";
import Products from "./pages/products/products";
import ProductDetail from "./pages/product-detail/product-detail";
import Profile from "./pages/profile/profile";
import Login from "./pages/login/login";
import Register from "./pages/register/register";

const routes = createBrowserRouter(
  createRoutesFromElements(
    <Route path="/" element={<Root />}>
      <Route index={true} element={<Home />} />
      <Route path="products" element={<Products />} />
      <Route path="products/:productId" element={<ProductDetail />} />
      <Route path="articles/:articleId" element={<ArticleDetail />} />
      <Route element={<PrivateRoute />}>
        <Route path="profile" element={<Profile />} />
      </Route>
      <Route path="login" element={<Login />} />
      <Route path="register" element={<Register />} />
    </Route>
  )
);

const App = () => {
  return <RouterProvider router={routes} />;
};

export default App;
