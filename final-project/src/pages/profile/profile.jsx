import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router";

import { selectedUserInfo } from "../../store/auth/auth.selector";
import { logout } from "../../store/auth/auth.slice";

const Profile = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const userInfo = useSelector(selectedUserInfo);

  const handleLogout = () => {
    dispatch(logout());
    navigate("/login", { replace: true });
  };

  return (
    <section className="h-screen grid place-content-center">
      <div className="shadow-md rounded-md px-4 py-6 w-[300px] flex flex-col items-center">
        <img
          src="https://placekitten.com/g/600/600"
          alt="user-info"
          className="w-[200px] h-[200px] object-cover rounded-full border-4 border-gray-800"
        />
        <div className="space-y-1 mt-4">
          <h2 className="font-semibold text-xl">Name: {userInfo.name}</h2>
          <p>Email: {userInfo.email}</p>
          <p>Username: {userInfo.name}</p>
          <button
            onClick={handleLogout}
            className="w-full px-4 py-2 rounded-md text-white text-md font-medium bg-purple-600 hover:bg-purple-700"
          >
            Logout
          </button>
        </div>
      </div>
    </section>
  );
};

export default Profile;
