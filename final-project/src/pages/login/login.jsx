import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router";

import { useLoginMutation } from "../../store";
import { setCredentials, setToken } from "../../store/auth/auth.slice";
import { selectedUserInfo } from "../../store/auth/auth.selector";

const Login = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const [login, { isLoading }] = useLoginMutation();

  const userInfo = useSelector(selectedUserInfo);

  useEffect(() => {
    if (userInfo) {
      navigate("/");
    }
  }, [navigate, userInfo]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const res = await login({ email, password }).unwrap();

      dispatch(setCredentials(res.data.user));
      dispatch(setToken(res.data.token));

      navigate("/");
    } catch (error) {
      console.log(error?.data?.message || error);
    }
  };

  return (
    <div className="shadow-md border p-6 rounded-md">
      <h2 className="my-4 text-center font-semibold text-xl">Sign In</h2>
      <form onSubmit={handleSubmit} className="space-y-4">
        <div className="flex flex-col w-full space-y-2">
          <label htmlFor="">E-mail</label>
          <input
            type="text"
            value={email}
            placeholder="johndoe@gmail.com"
            onChange={(e) => setEmail(e.target.value)}
            className="w-full outline-none ring-1 ring-blue-500 px-4 py-2 rounded-md"
          />
        </div>
        <div className="flex flex-col w-full space-y-2">
          <label htmlFor="">Password</label>
          <input
            type="password"
            value={password}
            placeholder="*******"
            onChange={(e) => setPassword(e.target.value)}
            className="w-full outline-none ring-1 ring-blue-500 px-4 py-2 rounded-md"
          />
        </div>
        <button
          type="submit"
          disabled={isLoading}
          className="px-4 py-2 rounded-md w-full text-white font-semibold bg-blue-500"
        >
          Login
        </button>
      </form>
    </div>
  );
};

export default Login;
