import { Link, useParams } from "react-router-dom";
import { useGetProductByIdQuery } from "../../store";

const ProductDetail = () => {
  const { productId } = useParams();

  const { data, isLoading, error } = useGetProductByIdQuery(productId);

  const product = data?.data;

  if (isLoading) {
    return <div>Loading content...</div>;
  }

  if (error) {
    return <div>Error {error.data?.message}</div>;
  }

  return (
    <section className="my-10">
      <div className="shadow-md w-full md:flex flex-row p-4 gap-x-4">
        <img
          src={product.image_url}
          alt={`${product.name}`}
          className="md:w-[250px] object-cover rounded-md"
        />
        <div className="space-y-1 mt-4 md:mt-0">
          <h2 className="text-xl font-semibold capitalize">{product.name}</h2>
          <p>{product.is_diskon && product.harga_diskon_display}</p>
          <p className={`${product.is_diskon && "text-red-500 line-through"}`}>
            {product.harga_display}
          </p>
          <p>Stock: {product.stock}</p>
          <p className="text-justify">
            <span className="text-purple-500 capitalize">
              {product.description}
            </span>{" "}
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem
            dignissimos quisquam nulla eveniet perspiciatis, facere, corrupti
            odio id omnis nihil consequuntur dolore repellendus suscipit quae
            ducimus error blanditiis ipsum quasi?
          </p>
          <div>
            <Link
              to="/products"
              className="w-full px-4 py-2 rounded-md text-white font-semibold bg-blue-500 hover:bg-blue-600"
            >
              Go to products
            </Link>
          </div>
        </div>
      </div>
    </section>
  );
};

export default ProductDetail;
