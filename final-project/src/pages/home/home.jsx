import { Link } from "react-router-dom";
import { useGetArticleQuery } from "../../store";

const Home = () => {
  const { data, isLoading, error } = useGetArticleQuery();

  const articles = data?.data;

  if (isLoading) {
    return <div>Loading content...</div>;
  }

  if (error) {
    return <div>Error {error.data?.message}</div>;
  }

  return (
    <section>
      <div className="flex gap-x-3 my-8">
        <Link
          to="/products"
          className="px-4 py-2 rounded-md text-white font-semibold bg-blue-500 hover:bg-blue-600"
        >
          Products
        </Link>
      </div>

      <div className="flex flex-row flex-wrap gap-y-4">
        {articles.map((article) => (
          <Link key={article.id} to={`/articles/${article.id}`}>
            <div className="shadow-md w-full md:flex flex-row items-center p-4 gap-x-4">
              <img
                src={article.image_url}
                alt={`${article.name}`}
                className="md:w-[250px] object-cover rounded-md"
              />
              <div className="space-y-2 mt-4 md:mt-0">
                <h2
                  className={`text-xl font-semibold capitalize ${
                    article.highlight && "text-red-500"
                  }`}
                >
                  {article.name}
                </h2>
                <p className="text-justify">
                  <span className="text-purple-500 capitalize">
                    {article.content}
                  </span>{" "}
                  Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                  Ducimus consectetur eveniet ea sed ullam tempora, at delectus
                  praesentium alias, quam non voluptate ratione inventore,
                  incidunt dolorum deserunt numquam possimus vero.
                </p>
              </div>
            </div>
          </Link>
        ))}
      </div>
    </section>
  );
};

export default Home;
