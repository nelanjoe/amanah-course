import { Link, useParams } from "react-router-dom";
import { useGetArticleByIdQuery } from "../../store";

const ArticleDetail = () => {
  const { articleId } = useParams();

  const { data, isLoading, error } = useGetArticleByIdQuery(articleId);
  const article = data?.data;

  if (isLoading) {
    return <div>Loading content...</div>;
  }

  if (error) {
    return <div>Error {error.data?.message}</div>;
  }

  return (
    <section className="my-12">
      <div className="my-10">
        <h2 className="text-center text-2xl font-semibold">Article Detail</h2>
      </div>
      <div className="shadow-md w-full md:flex flex-row items-center p-4 gap-x-4">
        <img
          src={article.image_url}
          alt={`${article.name}`}
          className="md:w-[250px] object-cover rounded-md"
        />
        <div className="space-y-2 mt-4 md:mt-0">
          <h2
            className={`text-xl font-semibold capitalize ${
              article.highlight && "text-red-500"
            }`}
          >
            {article.name}
          </h2>
          <p className="text-justify">
            <span className="text-purple-500 capitalize">
              {article.content}
            </span>{" "}
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ducimus
            consectetur eveniet ea sed ullam tempora, at delectus praesentium
            alias, quam non voluptate ratione inventore, incidunt dolorum
            deserunt numquam possimus vero.
          </p>
          <div>
            <Link
              to="/"
              className="w-full px-4 py-2 rounded-md text-white font-semibold bg-blue-500 hover:bg-blue-600"
            >
              Go to home
            </Link>
          </div>
        </div>
      </div>
    </section>
  );
};

export default ArticleDetail;
