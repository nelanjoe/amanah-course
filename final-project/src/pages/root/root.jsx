import { Outlet } from "react-router";

import Header from "../../components/header/header";

const Root = () => {
  return (
    <>
      <Header />
      <main className="max-w-5xl mx-4 md:mx-auto mt-6 h-screen">
        <Outlet />
      </main>
    </>
  );
};

export default Root;
