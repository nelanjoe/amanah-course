import { Link } from "react-router-dom";
import { useGetProductQuery } from "../../store";

const Products = () => {
  const { data, isLoading, error } = useGetProductQuery();

  const products = data?.data;

  if (isLoading) {
    return <div>Loading content...</div>;
  }

  if (error) {
    return <div>Error {error.data?.message}</div>;
  }

  return (
    <div>
      <div className="flex flex-row flex-wrap gap-y-6">
        {products.map((product) => (
          <Link key={product.id} to={`/products/${product.id}`}>
            <div className="shadow-md w-full md:flex flex-row p-4 gap-x-4">
              <img
                src={product.image_url}
                alt={`${product.name}`}
                className="md:w-[250px] object-cover rounded-md"
              />
              <div className="space-y-2 mt-4 md:mt-0">
                <h2 className="text-xl font-semibold capitalize">
                  {product.name}
                </h2>
                <p>{product.is_diskon && product.harga_diskon_display}</p>
                <p
                  className={`${
                    product.is_diskon && "text-red-500 line-through"
                  }`}
                >
                  {product.harga_display}
                </p>
                <p>Stock: {product.stock}</p>
                <p className="text-justify">
                  <span className="text-purple-500 capitalize">
                    {product.description}
                  </span>{" "}
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                  Autem dignissimos quisquam nulla eveniet perspiciatis, facere,
                  corrupti odio id omnis nihil consequuntur dolore repellendus
                  suscipit quae ducimus error blanditiis ipsum quasi?
                </p>
              </div>
            </div>
          </Link>
        ))}
      </div>
    </div>
  );
};

export default Products;
