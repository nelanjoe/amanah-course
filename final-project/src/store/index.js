import { configureStore } from "@reduxjs/toolkit";
import { apisSlice } from "./apis/apis.slice";
import { authReducer } from "./auth/auth.slice";

export const store = configureStore({
  reducer: {
    [apisSlice.reducerPath]: apisSlice.reducer,
    auth: authReducer,
  },
  middleware: (getDefaultMiddleWare) => {
    return getDefaultMiddleWare().concat(apisSlice.middleware);
  },
});

export { useLoginMutation } from "./auth/auth.api";

export {
  useGetArticleQuery,
  useGetArticleByIdQuery,
} from "./article/article.api";

export {
  useGetProductQuery,
  useGetProductByIdQuery,
} from "./product/product.api";
