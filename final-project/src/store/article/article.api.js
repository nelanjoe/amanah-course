import { apisSlice } from "../apis/apis.slice";

export const articleApi = apisSlice.injectEndpoints({
  endpoints: (builder) => ({
    getArticle: builder.query({
      query: () => {
        return {
          url: "/articles",
          method: "GET",
        };
      },
      transformErrorResponse: (response) => response,
    }),
    getArticleById: builder.query({
      query: (articleId) => {
        return {
          url: `/articles/${articleId}`,
          method: "GET",
        };
      },
    }),
  }),
});

export const { useGetArticleQuery, useGetArticleByIdQuery } = articleApi;
