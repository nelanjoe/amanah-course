import { apisSlice } from "../apis/apis.slice";

export const productApi = apisSlice.injectEndpoints({
  endpoints: (builder) => ({
    getProduct: builder.query({
      query: () => {
        return {
          url: "/products",
          method: "GET",
        };
      },
      transformErrorResponse: (response) => response,
    }),
    getProductById: builder.query({
      query: (productId) => {
        return {
          url: `/products/${productId}`,
          method: "GET",
        };
      },
      transformErrorResponse: (response) => response,
    }),
  }),
});

export const { useGetProductQuery, useGetProductByIdQuery } = productApi;
